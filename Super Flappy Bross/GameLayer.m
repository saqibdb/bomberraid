//
//  GameLayer.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "GameLayer.h"
#import "EndLayer.h"
#import "Global.h"
#import "Coin.h"
#import "Fish.h"
#import "Explosion.h"
#import "AppDelegate.h"
#import "Gun.h"


@implementation GameLayer
+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	GameLayer *layer = [GameLayer node];
	[scene addChild: layer];
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
        self.touchEnabled = YES;
        [self createBack];
        [self createMenu];
        [self initVar];
	}
	return self;
}
-(void) createBack {
    
   // AppController *app = (AppController *)[[UIApplication sharedApplication] delegate];
//    [app hideIAdBanner];
    
    newSprite(@"game_bg", G_SWIDTH/2, G_SHEIGHT/2, self, -1, RATIO_XY);

    m_spButtom[0] = newSprite(@"bottom", G_SWIDTH/2, getY(640 - 70), self, 1, RATIO_XY);
    m_spButtom[0].anchorPoint = ccp(0.5f, 1.0);
    m_spButtom[1] = newSprite(@"bottom", G_SWIDTH*3/2, getY(640 - 70), self, 1, RATIO_XY);
    m_spButtom[1].anchorPoint = ccp(0.5f, 1.0);
    
    
//    m_spTop[0] = newSprite(@"bottom", G_SWIDTH/2, getY(70), self, 1, RATIO_XY);
//    m_spTop[0].anchorPoint = ccp(0.5f, 0.0);
//    m_spTop[1] = newSprite(@"bottom", G_SWIDTH*3/2, getY(70), self, 1, RATIO_XY);
//    m_spTop[1].anchorPoint = ccp(0.5f, 0.0);
    
    m_spTap = newSprite(@"tap_start", G_SWIDTH/2, getY(450), self, 0, RATIO_XY);
    [m_spTap runAction:[CCRepeatForever actionWithAction:[CCBlink actionWithDuration:1 blinks:2]]];
    
    //CCSprite *coin = newSprite(@"coin", getX(35), getY(30), self, 1, RATIO_Y);
    //coin.anchorPoint = ccp(0, 0.5f);
    
//    [coin runAction:[CCRepeatForever actionWithAction:[CCBlink actionWithDuration:0.5 blinks:1]]];
    
    //m_lbCoin = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", g_nCoins] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    //m_lbCoin.anchorPoint = ccp(0, 0.5f);
    //m_lbCoin.position = ccp(getX(100), getY(30));
    //[self addChild:m_lbCoin z:5];
    
    m_lbStage = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"STAGE:%d", g_nStage] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    m_lbStage.anchorPoint = ccp(0.5f, 0.5f);
    m_lbStage.position = ccp(getX(600), getY(30));
    [self addChild:m_lbStage z:5];
    
    m_lbDistance = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%dm", m_nDistance] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    m_lbDistance.anchorPoint = ccp(1.0f, 0.5f);
    m_lbDistance.position = ccp(G_SWIDTH - getX(30), getY(30));
    [self addChild:m_lbDistance z:5];
    
    m_lbScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"SCORE:%d", 0] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    m_lbScore.anchorPoint = ccp(0.0f, 0.5f);
    m_lbScore.position = ccp(0, getY(30));
    [self addChild:m_lbScore z:5];
    
    
   
    
    // for pointing lives
//    newSprite(@"live", getX(300), getY(30), self, 5, RATIO_Y);
//    m_lbLive = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"x%d", g_nLives] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
//    m_lbLive.anchorPoint = ccp(0.f, 0.5f);
//    m_lbLive.position = ccp(getX(350), getY(30));
//    [self addChild:m_lbLive z:5];
    
    m_lbStageTitle = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"STAGE %d", g_nStage] fontName:@"Kemco Pixel" fontSize:70*G_SCALEY/g_fScaleR];
    m_lbStageTitle.anchorPoint = ccp(0.5f, 0.5f);
    m_lbStageTitle.position = ccp(G_SWIDTH/2, getY(180));
    [self addChild:m_lbStageTitle z:100];
    m_lbStageTitle.visible = NO;
    
    
    int nStage = [[NSUserDefaults standardUserDefaults] integerForKey:@"stage"];
    int nDistance = [[NSUserDefaults standardUserDefaults] integerForKey:@"distance"];
    int nScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"score"];
    
    m_lbHighScore = [CCLabelTTF labelWithString:@"HIGH SCORE:" fontName:@"Kemco Pixel" fontSize:50*G_SCALEY/g_fScaleR];
    m_lbHighScore.anchorPoint = ccp(0.5f, 0.5f);
    m_lbHighScore.position = ccp(G_SWIDTH/2, getY(150));
    [self addChild:m_lbHighScore z:100];
    
    m_lbHighStage = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"STAGE: %d", nStage] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    m_lbHighStage.anchorPoint = ccp(0.5f, 0.5f);
    m_lbHighStage.position = ccp(G_SWIDTH/2, getY(230));
    [self addChild:m_lbHighStage z:100];
    
    m_lbHighDistance = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"DISTANCE: %d", nDistance] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    m_lbHighDistance.anchorPoint = ccp(0.5f, 0.5f);
    m_lbHighDistance.position = ccp(G_SWIDTH/2, getY(280));
    [self addChild:m_lbHighDistance z:100];
    
    m_lbtopScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Score: %d", nScore] fontName:@"Kemco Pixel" fontSize:40*G_SCALEY/g_fScaleR];
    m_lbtopScore.anchorPoint = ccp(0.5f, 0.5f);
    m_lbtopScore.position = ccp(G_SWIDTH/2, getY(330));
    [self addChild:m_lbtopScore z:100];
    
    
    
}
-(void) createMenu
{
    
}
-(void)initVar {
    
    m_aryWeeds = [[NSMutableArray alloc] init];
    m_aryObstacles = [[NSMutableArray alloc] init];
    m_aryCoins = [[NSMutableArray alloc] init];
    m_aryFish = [[NSMutableArray alloc] init];
    m_aryExplosion = [[NSMutableArray alloc] init];
    
    m_nTimer = 0;
    m_bStart = NO;
    m_spHero = newSprite(@"hero_", getX(200), getY(640 - 70), self, 1, RATIO_Y);
    m_spHero.anchorPoint = ccp(0, 0.5);
//    m_spHero.rotation = -90;
    //m_fSpeedX = 10*G_SCALEX/g_fScaleR;
    
    m_fSpeedX = 7*G_SCALEX/g_fScaleR;
    
    bexplosionFirst= TRUE;
    m_fExlposionY = 4*G_SCALEY/g_fScaleR;
    
    // for rotation
    m_tappedTime = 0;
    m_RotateYUp = 2.5;
    m_RotateYDown = 3;
    
    
}
-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!m_bStart) {
        m_bStart = YES;
        [m_spHero runAction:[CCSequence actions:[CCSpawn actions:[CCMoveTo actionWithDuration:0.2f position:ccp(getX(200), G_SHEIGHT/2)], [CCRotateTo actionWithDuration:1.0f angle:-5], nil],[CCCallFunc actionWithTarget:self selector:@selector(gameStart)], nil]];
//        [self gameStart];
        m_spTmp = newSprite(@"hero_", getX(230), G_SHEIGHT/2, self, 0, RATIO_Y);
        m_spTmp.opacity = 0;
        m_spTmp.scale = G_SCALEY * 0.7;
        m_lbStageTitle.visible = YES;
        m_lbHighStage.visible = NO;
        m_lbHighDistance.visible = NO;
        m_lbHighScore.visible = NO;
        m_lbtopScore.visible = NO;
        return;
    }
    
    m_lbStageTitle.visible = NO;
    m_fSpeedY =6.5 * G_SCALEY / g_fScaleR;
    m_iPastTime = 0;
    
    // for rotation
    m_tapped = true;
    m_tappedTime = 10;
    
    
    
    
}
-(void)gameStart
{
    [m_spTap stopAllActions];
    m_spTap.visible = NO;
   [self schedule:@selector(onTimer:) interval:0.02];
    
    m_spHero.anchorPoint = ccp(0.5, 0.5);
    //[m_spHero runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:g_frmHero]]];
}


-(void) onTimer:(ccTime)dt
{
    
    CGSize winSize = [[CCDirector sharedDirector]winSize];
    
    //setting postion
    m_fSpeedY -= 0.06 * m_iPastTime;
    m_iPastTime++;
    m_spHero.position = ccpAdd(m_spHero.position, ccp(0, m_fSpeedY));
    
    
    if(m_tapped)
    {       m_tappedTime --;
        float rotation=-1* m_RotateYUp;
        
        
        
        m_spHero.rotation = m_spHero.rotation + rotation ;
        
        if(m_spHero.rotation <= -5)
        {
            m_spHero.rotation = -5;
        }
        
        if(m_tappedTime == 0)
        {
            m_tapped = false;
        }
    }
    else
    {
        m_spHero.rotation = m_spHero.rotation + m_RotateYDown;
        if(m_spHero.rotation >= 0)
        {
            m_spHero.rotation = 0;
        }
        
    }
    
    
    
    
    
    m_spTmp.position = ccpAdd(m_spTmp.position, ccp(0, m_fSpeedY));

    // for collision with objects
    if((m_spTmp.boundingBox.origin.y + m_spTmp.boundingBox.size.height) > getY(90)){
        m_fSpeedY = 0;
    }
    else if(m_spTmp.boundingBox.origin.y < getY(550)){
        if(g_nLives > 0) {
            m_spTmp.position = ccp(getX(230), G_SHEIGHT/2);
            m_spHero.position = ccp(getX(230), G_SHEIGHT/2);
        }
        [self onDeath];
    }
    
    for (int i = 0; i < 2; i++) {
        if (m_spButtom[i].position.x<-G_SWIDTH/2) {
          
            m_spButtom[i].position = ccpAdd(m_spButtom[i].position, ccp(G_SWIDTH*2, 0));;
        }
       
        m_spButtom[i].position = ccpAdd(m_spButtom[i].position, ccp(-m_fSpeedX,0));
        
    }
    
    /*
    // for fish
    if (m_nDistance % 20 == 1 && m_nTimer % 10 == 0) {
        Fish *fish = [Fish shareFish];
        fish.position = ccp(getX(1200), getY(200 + arc4random()%200));
        [self addChild:fish z:5];
        [m_aryFish addObject:fish];
    }
    for (int i = [m_aryFish count] -1; i>=0; i--)
    {
        Fish *fish = [m_aryFish objectAtIndex:i];
        fish.position = ccpSub(fish.position, ccp(2*10*G_SCALEX/g_fScaleR, 0));
        if (CGRectIntersectsRect(m_spTmp.boundingBox, fish.boundingBox)){
            if (!m_bBlink) {
                [self onDeath];
            }
            
        }
        if (fish.position.x < -50) {
            [m_aryFish removeObject:fish];
            [self removeObjectFromLayer:fish];
        }
    }
     
     */
    
    
    
    
    
    // collision with explosion
    if(m_aryObstacles != nil && [m_aryObstacles count] > 0)
    {
        for(int i=0; i<[m_aryObstacles count]; i++){
        
            
          
            Gun * gun = [m_aryObstacles objectAtIndex:i];
            
            if((gun.explosionArray != nil ) && [gun.explosionArray count] > 0)
            {
                for ( int j = 0 ; j < [gun.explosionArray count] ; j++)
                {
                    CCSprite *exp = [gun.explosionArray objectAtIndex:j];
                   // exp.position = ccpSub(exp.position, ccp(m_fSpeedX, -m_fExlposionY));
                    exp.position = ccp(exp.position.x - m_fSpeedX, exp.position.y);
                    
                    
                    CGRect box = m_spTmp.boundingBox;
                    CGRect collisionRect = CGRectMake(box.origin.x, box.origin.y, box.size.width*80/100, box.size.height*95/100);
                    
                    if (CGRectIntersectsRect(collisionRect, exp.boundingBox))
                    {
                        if (!m_bBlink)
                        {
                         [self onDeath];
                        }
                    
                    }
               
                    if (exp.position.x < - 100)
                    {
                        [gun.explosionArray removeObject:exp];
                        [self removeObjectFromLayer:exp];
                    }
                }
            }
        }
    }
    
    // collision with with obstacle
    for (int i = [m_aryObstacles count] -1; i>=0; i--)
    {
        Gun *obstacle = [m_aryObstacles objectAtIndex:i];
        obstacle.position = ccpSub(obstacle.position, ccp(m_fSpeedX, 0));
        
        //setting score
        if( obstacle.position.x <= m_spHero.position.x && obstacle.isScoreAdded == false)
        {
            obstacle.isScoreAdded = true;
            g_nScore ++ ;
            [m_lbScore setString:[NSString stringWithFormat:@"Score: %d", g_nScore]];
        }
        
        CGRect box = m_spTmp.boundingBox;
        CGRect collisionRect = CGRectMake(box.origin.x, box.origin.y, box.size.width*80/100, box.size.height*95/100);
        if (CGRectIntersectsRect(collisionRect, obstacle.boundingBox)){
            if (!m_bBlink) {
                [self onDeath];
            }

        }
        if (obstacle.position.x < -100) {
            [m_aryObstacles removeObject:obstacle];
            [self removeObjectFromLayer:obstacle];
        }
    }
    
    //creating guns
    
    CCSprite * lastObstacle = [m_aryObstacles lastObject];
    if( lastObstacle == nil || ( lastObstacle != nil && lastObstacle.position.x <= [[CCDirector sharedDirector]winSize].width*70/100))
  {
      
      
      int screenDiff = arc4random() % 40;
    
      if(screenDiff < 5)
      {
          screenDiff = 5;
      }
      
      float minHeight = m_spButtom[0].position.y + (winSize.height/2.0) *(screenDiff/100.0);
      
      Gun *gun = [[Gun alloc] initWithFile:@"obstacle.png"];
      setScale(gun, RATIO_Y);
    //  gun.position = ccp(winSize.width + gun.boundingBox.size.width, G_SHEIGHT/2 + (diff - 150) * G_SCALEY / g_fScaleR);
      gun.position = ccp(winSize.width + gun.boundingBox.size.width,minHeight /*G_SHEIGHT/2 + (diff - 150) * G_SCALEY / g_fScaleR*/);
      
    
      
      [self addChild:gun z:2];
      gun.anchorPoint = ccp(0.5f , 1);
      [m_aryObstacles addObject:gun];
  }
      
    
    if( m_aryObstacles != nil && [m_aryObstacles count] > 0)
    {
    
     
      //set explosion
      for( int i = 0 ; i < [m_aryObstacles count] ; i++)
      {
          Gun * gun = [m_aryObstacles objectAtIndex:i];
          
          
         
          
          if (gun.position.x <= winSize.width * 80/100 && gun.position.x >= winSize.width * 20/100)
          {
              
              gun.lastFiredTime = gun.lastFiredTime + 0.02;
            
             
            
              
              if( gun.didGunFired)
              {
                  continue;
              }
              
              // setting fire true
              gun.didGunFired = true;
              gun.lastFiredTime = 0;
              gun.fireTime = gun.fireTime + 0.3;
              
            
              CCSprite *sprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@.png",@"exp_15"]];
             
              
              float height = sprite.contentSize.height;
              
              
              float scale = m_spHero.scaleY;
              
              
              float randomNum = ((float)rand() / RAND_MAX) *6;
              
              if( randomNum < 3)
              {
                  randomNum = 3.0;
              }
              
              
              
               float gap = (m_spHero.contentSize.height*randomNum*scale);
              
              
              
              float randomCount = arc4random_uniform(3);
              float y = gun.position.y + height;
              int i = 0;
             while ( y < winSize.height)
             {
                 if( i == 0)
                 {
                     if(randomCount == i)
                     {
                         y = y + gap;
                     }
                     
                 CCSprite *exp = newSprite(@"exp_1", gun.position.x, y, self, 5, RATIO_Y);
                 [exp runAction:[CCAnimate actionWithAnimation:g_frmExplosion]];
                 [gun.explosionArray addObject:exp];
                     
                     
                 }
                 else if ( i == 1)
                 {
                     if(randomCount == i)
                     {
                         y = y + height + gap;
                     }
                     else
                     {
                         y = y + height;
                     }
                     CCSprite *exp2 = newSprite(@"exp_1", gun.position.x,y, self, 5, RATIO_Y);
                     [exp2 runAction:[CCAnimate actionWithAnimation:g_frmExplosion]];
                     [gun.explosionArray addObject:exp2];
                     
                 }
                 else
                 {
                     
                     if(randomCount == i)
                     {
                         y = y + height + gap;
                     }
                     else
                     {
                         y = y + height;
                     }
                     CCSprite *exp3 = newSprite(@"exp_1", gun.position.x, y , self, 5, RATIO_Y);
                     [exp3 runAction:[CCAnimate actionWithAnimation:g_frmExplosion]];
                     [gun.explosionArray addObject:exp3];
                     
                 }
                 i++;
                 
             }
              
              
             
              
              
              
              
              

          }
          
      }
      
      
      
  }
    
    
    m_nTimer++;
    m_nDistance = m_nTimer/10;
    
    if(m_nTimer % 10 == 0)
    {
        m_fSpeedX = m_fSpeedX + 0.01;
    }
   

    if(m_nDistance % 200 == 0 && m_nTimer%10 == 0) {
        CCSprite *star = newSprite(@"star", getX(1250 + arc4random()%100), G_SHEIGHT/2, self, 5, RATIO_Y);
        star.tag = 100;
        star.scale = 1.5 * G_SCALEY;
        [star runAction:[CCRepeatForever actionWithAction:[CCBlink actionWithDuration:0.5 blinks:1]]];
        [m_aryCoins addObject:star];
    }
    
    
    
    
    if (m_nDistance % 50 == 49 && m_nTimer%10 == 0) {
        g_nStage++;
        m_fSpeedX = m_fSpeedX + 0.05;
        [m_lbStage setString:[NSString stringWithFormat:@"STAGE:%d", g_nStage]];
        m_lbStageTitle.visible = YES;
        [m_lbStageTitle setString:[NSString stringWithFormat:@"STAGE: %d", g_nStage]];
        [[SimpleAudioEngine sharedEngine] playEffect:@"new stage.mp3"];
        m_nDistance = 0;
        m_fExlposionY = m_fExlposionY + m_fExlposionY*5/100;
        
    }
    [m_lbDistance setString:[NSString stringWithFormat:@"%dm", m_nDistance]];
}

-(void) onDeath
{
    g_nLives --;
    if(g_nLives == -1){
//        [[SimpleAudioEngine sharedEngine] playEffect:@"dead.mp3"];
        [self unschedule:@selector(onTimer:)];
        [m_spHero runAction:[CCSequence actions:[CCSpawn actions:[CCMoveTo actionWithDuration:0.5f position:ccp(m_spHero.position.x, getY(550))], [CCRotateTo actionWithDuration:0.5f angle:70], nil],[CCCallFunc actionWithTarget:self selector:@selector(gameOver)], nil]];
        return;
    }
    else
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"ouch.mp3"];
        m_bBlink = YES;
//        m_spHero.position = ccp(getX(200), G_SHEIGHT/2);
//        m_spTmp.position = ccp(getX(230), G_SHEIGHT/2);
        [m_spHero runAction:[CCSequence actions:[CCBlink actionWithDuration:4.f blinks:50],[CCCallFunc actionWithTarget:self selector:@selector(endBlinkUp)], nil]];
    }
    [m_lbLive setString:[NSString stringWithFormat:@"x%d", g_nLives]];
}
-(void) endBlinkUp {
    m_spHero.visible = YES;
    m_bBlink = NO;
}
-(void) gameOver{
//    [[CCDirector sharedDirector] replaceScene:[EndLayer node]];
    [[SimpleAudioEngine sharedEngine] playEffect:@"dead.mp3"];
    m_lbStageTitle.visible = NO;
    g_nDistance = m_nDistance;
    self.touchEnabled = NO;
    [m_spHero stopAllActions];
    [self unscheduleAllSelectors];
    [self addChild:[EndLayer node] z:100];
    
}
-(void) addCoin
{
    float a = 170;
    if (arc4random()%2 ==0) {
        a = 570 - 100;
    }
    for (int i = 0; i < 4; i++) {
        Coin *coin = [Coin shareCoin];
        coin.position = ccp((1250 +i *70)*G_SCALEX/g_fScaleR  , getY(a));
        [self addChild:coin z:5];
        [m_aryCoins addObject:coin];
    }
}
#define OBSTACLE_GAP    100
-(void)removeObjectFromLayer:(CCNode *)sender
{
    [sender removeFromParentAndCleanup:YES];
}
@end
