//
//  GameConfig.h
//  shootingbird
//
//  Created by limcholsung on 3/11/14.
//  Copyright 2014 org. All rights reserved.
//

#ifndef _GAME_CONFIG_H
#define _GAME_CONFIG_H
#import "cocos2d.h"
#import "Constants.h"

#define TEXT_TAG    1
#define FX_BTN() [[SimpleAudioEngine sharedEngine] playEffect:@"button.mp3"];

typedef enum
{
    D_EASY = 0,
    D_MEDIUM,
    D_HARD,
}DIFFICULTY;

extern BOOL g_bMusicMute;
extern BOOL g_bSoundMute;
extern int g_nScore;
extern int g_nCoins;
extern int g_nLives;
extern int g_nStage;
extern int g_nDistance;





extern CCAnimation *g_frmHero;
extern CCAnimation *g_frmExplosion;

#endif
