//
//  GameConfig.m
//  shootingbird
//
//  Created by limcholsung on 3/11/14.
//  Copyright 2014 org. All rights reserved.
//

#import "GameConfig.h"

BOOL g_bMusicMute = NO;
BOOL g_bIsTimeMode = NO;
BOOL g_bSoundMute = NO;
int g_nTotalScore = 0;
int g_nScore = 0;
int g_nCoins = 0;
int g_nStage = 1;
int g_nLives = 3;
int g_nDistance = 0;


CCAnimation *g_frmHero;

CCAnimation *g_frmExplosion;


