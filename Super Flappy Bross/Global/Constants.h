//
//  Constants.h
//  shootingbird
//
//  Created by limcholsung on 3/11/14.
//  Copyright 2014 org. All rights reserved.
//

#ifndef MrDentist_Constants_h
#define MrDentist_Constants_h

#define OBSTACLE_COUNT 3
#define RATE_ID                     @"887436634"
#define RATE_MESSAGE                @"If you like Cowbunga, please rate it! Thanks!"
#define RATE_TITLE                  @"Cowbunga"

#define LEADERBOARD_ID              @"bestscore"
#define REVMOB_APP_ID               @"53a710c47cb78fb706e46ec2"
#define REVMOB_BANNER_ENABLE        NO
#define CHARTBOOST_APP_ID           @"53a7118e1873da56b1030cb4"
#define CHARTBOOST_APP_SIGNATURE    @"d8abe6e921a2ad6a8ca97538f5afce508d121580"

#define EMAIL_SUBJECT               @"flaming owl"
#define EMAIL_BODY                  @"Check out the Die Birdy Die"

#define GAME_FBTEXT                 @"Check out the Die Birdy Die!"
#define GAME_TWITTERTEXT            @"Check out the Die Birdy Die"

#define IAP_Q_LIVES                 @"Are you sure you want to get 3 lives and continue game for $0.99?"
#define IAP_LIVES                   @"threeextralives"

#define DEBUG_MODE                  NO
#define ENABLE_LIVES                YES
#define LIVES_COUNT                 


#endif
