//
//  Coin.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "Coin.h"
#import "Global.h"


@implementation Coin
+(Coin *)shareCoin{
    Coin *coin = [[Coin alloc] initCoin];
    return coin;
}
-(id)initCoin
{
    if(self = [super initWithFile:@"coin.png"]){
        [self createCoin];
    }
    return self;
}
-(void) createCoin
{
    self.scale = G_SCALEY;
    [self runAction:[CCRepeatForever actionWithAction:[CCSequence actions:[CCScaleTo actionWithDuration:0.2f scaleX:0 scaleY:G_SCALEY],[CCCallFuncN actionWithTarget:self selector:@selector(onFlipX)],[CCScaleTo actionWithDuration:0.2f scaleX:-G_SCALEY scaleY:G_SCALEY], [CCScaleTo actionWithDuration:0.2f scaleX:0 scaleY:G_SCALEY],[CCCallFuncN actionWithTarget:self selector:@selector(onFlipX)],[CCScaleTo actionWithDuration:0.2f scaleX:G_SCALEY scaleY:G_SCALEY],nil]]];
}
-(void)onFlipX
{
    if(self.flipX)self.flipX = NO;
    else self.flipX = YES;
}
@end
