//
//  Gun.m
//  Super Flappy Bross
//
//  Created by Hamza Temp on 10/10/2017.
//  Copyright © 2017 limcholsung. All rights reserved.
//

#import "Gun.h"

#import "Global.h"


@implementation Gun


@synthesize explosionArray,lastFiredTime,isScoreAdded,fireTime,numberOfFires,didGunFired;

-(id)initWithFile:(NSString *)filename {
    if(self = [super initWithFile:filename])
    {
        
        explosionArray = [[NSMutableArray alloc]init];
        lastFiredTime = 10;
        isScoreAdded = false;
        fireTime = 0.2;
        numberOfFires = 0;
        didGunFired = false;
        
        
    }
    return self;
}

/*
-(Explosion*) createShot
{
    
    Explosion *explosion = nil;
    if(self.explosionArray && [self.explosionArray count] ==0)
    {
    explosion = [[Explosion alloc]initWithFile:@"exp_1.png"];
   [explosionArray addObject:explosion];
   
    }
     return explosion;
    
}*/

@end
