//
//  GameLayer.h
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameLayer : CCLayer {

    CCSprite *m_spTap;
    CCSprite *m_spHero;
    CCSprite *m_spTmp;
    
    BOOL m_bStart;
    BOOL m_bBlink;
    int m_nTimer;
    float m_fSpeedY;
    float m_fSpeedX;
    float m_fExlposionY;
    float m_iPastTime;
    int m_nDistance;
    
    
    
    BOOL m_tapped;
    float m_RotateYUp;
    float m_RotateYDown;
    float m_tappedTime;

    
    BOOL bexplosionFirst;
    
    // class member array variables
    NSMutableArray *m_aryWeeds;
    NSMutableArray *m_aryObstacles;
    NSMutableArray *m_aryCoins;
    NSMutableArray *m_aryFish;
    NSMutableArray *m_aryExplosion;
    
    // action object variables
    CCAction *m_actAniSwim;
    
    // lables
    CCLabelTTF *m_lbCoin;
    CCLabelTTF *m_lbStage;
    CCLabelTTF *m_lbDistance;
    CCLabelTTF *m_lbLive;
    CCLabelTTF *m_lbStageTitle;
    CCLabelTTF *m_lbScore;
    
    CCSprite *m_spButtom[2];
    CCSprite *m_spTop[2];
    
    
    //// for showing
    CCLabelTTF *m_lbHighScore;
    CCLabelTTF *m_lbHighStage;
    CCLabelTTF *m_lbHighDistance;
    CCLabelTTF *m_lbtopScore;

}
+(CCScene *) scene;
@end
