//
//  EndLayer.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "EndLayer.h"
#import "GameLayer.h"
#import "MenuLayer.h"
#import "AppDelegate.h"
#import "Global.h"

@implementation EndLayer
+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	EndLayer *layer = [EndLayer node];
	[scene addChild: layer];
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
//        AppController *app = (AppController *)[[UIApplication sharedApplication] delegate];
//        [app ShowIAdBanner];
        self.touchEnabled = YES;
        [self createBack];
        [self createMenu];
	}
	return self;
}
-(void) createBack {
    
    newSprite(@"game_over", G_SWIDTH/2, getY(200), self, 0, RATIO_Y);
    
    CCLabelTTF *lbStage = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"STAGE: %d", g_nStage] fontName:@"Kemco Pixel" fontSize:50*G_SCALEX/g_fScaleR];
    lbStage.position = ccp(G_SWIDTH/2, getY(300));
    [self addChild:lbStage];
    
    CCLabelTTF *lbDistance = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"DISTANCE: %d",g_nDistance] fontName:@"Kemco Pixel" fontSize:50*G_SCALEX/g_fScaleR];
    lbDistance.position = ccp(G_SWIDTH/2, getY(370));
    [self addChild:lbDistance];
    
    
    CCLabelTTF *lbScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Score: %d",g_nScore] fontName:@"Kemco Pixel" fontSize:50*G_SCALEX/g_fScaleR];
    lbScore.position = ccp(G_SWIDTH/2, getY(440));
    [self addChild:lbScore];
    
    int nStage = [[NSUserDefaults standardUserDefaults] integerForKey:@"stage"];
    int nDistance = [[NSUserDefaults standardUserDefaults] integerForKey:@"distance"];
    int nScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"score"];
    
    if (g_nStage > nStage) {
        [[NSUserDefaults standardUserDefaults] setInteger:g_nStage forKey:@"stage"];
    }
    else if(g_nStage == nStage)
    {
        if (nDistance < g_nDistance) {
            [[NSUserDefaults standardUserDefaults] setInteger:g_nDistance forKey:@"distance"];
        }
    }
    
    
    if(g_nScore > nScore)
    {
        [[NSUserDefaults standardUserDefaults] setInteger:g_nScore forKey:@"score"];
    }
}
-(void) createMenu
{
    CCMenuItemImage *iShare = newButton(@"btn_share", G_SWIDTH/2, getY(500), self, @selector(onShare), YES, RATIO_Y);
    CCMenuItemImage *iRate = newButton(@"btn_rate", G_SWIDTH/2 + getX(150), getY(500), self, @selector(onRate), YES, RATIO_Y);
    CCMenu *menu = [CCMenu menuWithItems:iShare, nil];
    menu.position  = ccp(0, 0);
    [self addChild:menu z:0];
}
-(void) onRate {
    
}

-(void) onShare {
    AppController *app = (AppController *)[UIApplication sharedApplication].delegate;
    [app showActivityController];
}

-(void) onMenu {
    [[CCDirector sharedDirector] replaceScene:[MenuLayer node]];
}
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self onMenu];
}
@end
