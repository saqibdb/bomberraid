//
//  Fish.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "Fish.h"
#import "Global.h"


@implementation Fish
+(Fish *)shareFish {
    Fish *fish = [[Fish alloc] initFish];
    return fish;
}
-(id)initFish {
    if(self = [super initWithFile:@"fish.png"])
    {
        [self createFish];
    }
    return self;
}
-(void) createFish
{
    self.scale = G_SCALEY;
}
@end
