//
//  Gun.h
//  Super Flappy Bross
//
//  Created by Hamza Temp on 10/10/2017.
//  Copyright © 2017 limcholsung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Explosion.h"

@interface Gun : CCSprite

@property (assign) NSMutableArray *explosionArray;

@property float lastFiredTime;
@property float isScoreAdded;
@property float fireTime;
@property int numberOfFires;
@property bool didGunFired;

//-(Explosion*) createShot;


@end
