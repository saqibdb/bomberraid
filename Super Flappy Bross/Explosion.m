//
//  Fish.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "Explosion.h"
#import "Global.h"
#import "GameConfig.h"

@implementation Explosion
+(Explosion *)shareExplosion {
    Explosion *explosion = [[Explosion alloc] init];
    return explosion;
}
-(id)initWithFile:(NSString *)filename{
    if(self = [super initWithFile:filename])
    {
       
    }
    return self;
}
-(void) createExplosion
{
    self.scale = G_SCALEY;
    [self runAction:[CCAnimate actionWithAnimation:g_frmExplosion]];
}

@end
