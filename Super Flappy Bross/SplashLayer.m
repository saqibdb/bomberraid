//
//  SplashLayer.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/16/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "SplashLayer.h"
#import "MenuLayer.h"
#import "Global.h"


@implementation SplashLayer
+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	SplashLayer *layer = [SplashLayer node];
	[scene addChild: layer];
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
        self.touchEnabled = YES;
        [self createBack];
	}
	return self;
}
-(void)createBack{
    newSprite(@"splash_bg", G_SWIDTH/2, G_SHEIGHT/2, self, -1, RATIO_XY);
    [self schedule:@selector(onTimer:) interval:2.f];
}
-(void)onTimer:(ccTime)dt
{
    [self unscheduleAllSelectors];
    //[[CCDirector sharedDirector] replaceScene:[MenuLayer node]];
    
    [self showTutorial];
}

-(void) showTutorial{
    
    newSprite(@"game_bg", G_SWIDTH/2, G_SHEIGHT/2, self, -1, RATIO_XY);
    
    CCSprite *m_spButtom0 = newSprite(@"bottom", G_SWIDTH/2, getY(640 - 70), self, 1, RATIO_XY);
    m_spButtom0.anchorPoint = ccp(0.5f, 1.0);
    CCSprite *m_spButtom1 = newSprite(@"bottom", G_SWIDTH*3/2, getY(640 - 70), self, 1, RATIO_XY);
    m_spButtom1.anchorPoint = ccp(0.5f, 1.0);
    
    newSprite(@"hero_", getX(230), G_SHEIGHT/2, self, 2, RATIO_Y);
    
    int diff = arc4random() % 80;
    CCSprite *obstacleDown = newSprite(@"obstacle", getX(500), G_SHEIGHT/2 + (diff - 150) * G_SCALEY / g_fScaleR, self, 2, RATIO_Y);
    obstacleDown.anchorPoint = ccp(0.5f, 1);
    
    CCSprite *exp = newSprite(@"exp_12", getX(500), G_SHEIGHT/2+50, self, 5, RATIO_Y);
    //exp.scale = 2 * G_SCALEY;
//    [exp runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:g_frmExplosion]]];
    
    CCSprite *exp1 = newSprite(@"exp_12", getX(500), G_SHEIGHT/2+150, self, 5, RATIO_Y);
   // exp1.scale = 2 * G_SCALEY;
//    [exp1 runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:g_frmExplosion]]];
    
    
    CCLabelTTF *lblText = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"To fly the Bomber,\ncontinuously press the screen\nto keep it off the ground"] fontName:@"Kemco Pixel" fontSize:30*G_SCALEX/g_fScaleR];
    lblText.position = ccp(G_SWIDTH/2, G_SHEIGHT/2-100);
    [self addChild:lblText z:10];
    
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[CCDirector sharedDirector] replaceScene:[MenuLayer node]];
}

@end
