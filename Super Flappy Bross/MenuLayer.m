//
//  MenuLayer.m
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright 2014 limcholsung. All rights reserved.
//

#import "MenuLayer.h"
#import "GameLayer.h"
#import "Global.h"
#import "AppDelegate.h"


@implementation MenuLayer
+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	MenuLayer *layer = [MenuLayer node];
	[scene addChild: layer];
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
        self.touchEnabled = YES;
        [self createBack];
        [self createMenu];
	}
	return self;
}

-(void)createBack {

   //AppController *app = (AppController *)[[UIApplication sharedApplication] delegate];
//    [app ShowIAdBanner];
    
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"bg.mp3"];
    
    newSprite(@"game_bg", G_SWIDTH/2, G_SHEIGHT/2, self, -1, RATIO_XY);
    newSprite(@"game_logo", G_SWIDTH/2, getY(200), self, -1, RATIO_Y);
    
    newSprite(@"hero", G_SWIDTH/2, getY(492.5), self, -1, RATIO_Y);
    
    
}


-(void) createMenu
{
    
    CCMenuItemImage *fbBtn = newButton(@"fbicon", G_SWIDTH/2, getY(500), self, @selector(fbBtnPressed), YES, 1);
       CCMenu *menu = [CCMenu menuWithItems:fbBtn, nil];
    menu.position  = ccp(0, 0);
    
    CGSize size = [[CCDirector sharedDirector]winSize];
    
    CGSize  btnSize = fbBtn.boundingBox.size;
    fbBtn.position = ccp( size.width - btnSize.width/2 - size.width*(1.0/100.0), size.height - btnSize.height/2 - size.height*(1.0/100.0));
    
[self addChild:menu z:100];
}


- (void)fbBtnPressed
{
    NSURL *fanPageURL = [NSURL URLWithString:@"https://www.facebook.com/AppZuluGames/"];
    [[UIApplication sharedApplication] openURL:fanPageURL];
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
 
    g_nLives = 0;
    [[CCDirector sharedDirector] replaceScene:[GameLayer node]];
    
}

@end
