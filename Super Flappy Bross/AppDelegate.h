//
//  AppDelegate.h
//  Super Flappy Bross
//
//  Created by limcholsung on 9/8/14.
//  Copyright limcholsung 2014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@class MyiAd;

// Added only for iOS 6 support
@interface MyNavigationController : UINavigationController <CCDirectorDelegate>
@end

@interface AppController : NSObject <UIApplicationDelegate>
{
	UIWindow *window_;
	MyNavigationController *navController_;

    MyiAd   *mIAd;
    
	CCDirectorIOS	*director_;							// weak ref
}




@property (nonatomic, retain) UIWindow *window;
@property (readonly) MyNavigationController *navController;
@property (readonly) CCDirectorIOS *director;


-(void)ShowIAdBanner;
-(void)hideIAdBanner;
-(void)showActivityController;
-(void)showRate;

@end
